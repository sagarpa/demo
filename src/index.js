import React from 'react'
import ReactDOM from 'react-dom'
import ValidatorRouter from './components/router'
import registerServiceWorker from './registerServiceWorker'

//stylesheets
import './index.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/css/bootstrap-theme.css'

ReactDOM.render(<ValidatorRouter />, document.getElementById('root'))
registerServiceWorker()
