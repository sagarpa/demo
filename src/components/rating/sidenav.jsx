import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'

class SideNav extends Component {
  constructor(props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
    this.handleFinalize = this.handleFinalize.bind(this)
  }

  handleClick(e) {
    this.props.onCategoryClick(e.target.id)
  }

  handleFinalize() {
    this.props.onFinalize()
  }

  render() {
    const { categories, setActive } = this.props
    return (
      <div className="Sidenav">
        <ul className="Sidenav__Content">
          {categories.map((cat, index) => (
            <div
              key={index}
              className="SideNav__Heading SideNav__Heading--selected"
            >
              <li
                className={classNames({
                  SideNav__Active: setActive(cat)
                })}
                id={cat}
                onClick={this.handleClick}
              >
                {cat}
              </li>
              <hr />
            </div>
          ))}
          <li id="Finalize" onClick={this.handleFinalize}>
            Finalize Rating
          </li>
        </ul>
      </div>
    )
  }
}

SideNav.propTypes = {
  categories: PropTypes.array.isRequired,
  onCategoryClick: PropTypes.func.isRequired,
  onFinalize: PropTypes.func.isRequired
}

export default SideNav
