import React from 'react'
import PropTypes from 'prop-types'

import { SIDENAV_STRUCTURE } from './structures'

const Details = ({ categoryKey }) => {
  return (
    <div className="Details">
      <div className="Details__Content">
        <h2 className="Details__Heading">{categoryKey}</h2>
        {Object.keys(SIDENAV_STRUCTURE[categoryKey]).map((sub, index) => (
          <div key={index}>
            <h4 className="Details__Subheading">{sub}</h4>
            {Object.keys(SIDENAV_STRUCTURE[categoryKey][sub]).map(
              (name, index) => (
                <div className="row Details__Subsubheading" key={index}>
                  <h5 className="col-md-8">{name} :</h5>
                  <p className="col-md-4 Details__Subsubheading--p">
                    {SIDENAV_STRUCTURE[categoryKey][sub][name]}
                  </p>
                </div>
              )
            )}
          </div>
        ))}
      </div>
    </div>
  )
}

Details.propTypes = {
  categoryKey: PropTypes.string.isRequired
}

export default Details
