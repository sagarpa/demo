import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Modal, Alert, Button } from 'react-bootstrap'

class FinalizeModal extends Component {
  static propTypes = {
    showModal: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onClose: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)
    this.handleClose = this.handleClose.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit() {
    this.props.onSubmit()
  }

  handleClose() {
    this.props.onClose()
  }

  render() {
    const { showModal } = this.props
    return (
      <Modal show={showModal} onHide={this.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Alert bsStyle="danger" onDismiss>
            <h4>Finalize Ranking?</h4>
            <p>Once you finalize the Ranking it will be saved permanently.</p>
            <p>
              <Button bsStyle="danger" onClick={this.handleSubmit}>
                Continue
              </Button>
              <span> or </span>
              <Button onClick={this.handleClose}>Cancel</Button>
            </p>
          </Alert>
        </Modal.Body>
      </Modal>
    )
  }
}

export default FinalizeModal
