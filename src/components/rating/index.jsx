import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import SideNav from './sidenav'
import Details from './details'
import Aspect from './aspect'
import FinalizeModal from './modal'
import { SIDENAV_STRUCTURE } from './structures'

class Rating extends Component {
  constructor(props) {
    super(props)
    this.state = {
      categoryKey: "Representative's Details",
      showModal: false,
      gotoDash: false
    }

    this.handleCategoryClick = this.handleCategoryClick.bind(this)
    this.setActive = this.setActive.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.openModal = this.openModal.bind(this)
    this.redirectToDashboard = this.redirectToDashboard.bind(this)
  }

  handleCategoryClick(id) {
    this.setState({ categoryKey: id })
  }

  closeModal() {
    this.setState({ showModal: false })
  }

  openModal() {
    this.setState({ showModal: true })
  }

  redirectToDashboard() {
    this.setState({ gotoDash: true })
  }
  setActive(cat) {
    const { categoryKey } = this.state
    if (categoryKey === cat) return true
    return false
  }

  render() {
    const { categoryKey } = this.state
    if (!this.state.gotoDash)
      return (
        <div className="Ranking">
          <div className="Ranking__Content">
            <div className="row">
              <div className="col-md-3">
                <SideNav
                  setActive={this.setActive}
                  categories={Object.keys(SIDENAV_STRUCTURE)}
                  onFinalize={this.openModal}
                  onCategoryClick={this.handleCategoryClick}
                />
              </div>
              <div className="col-md-5">
                <Details categoryKey={categoryKey} />
              </div>
              <div className="col-md-4">
                <Aspect />
              </div>
              <FinalizeModal
                showModal={this.state.showModal}
                onSubmit={this.redirectToDashboard}
                onClose={this.closeModal}
              />
            </div>
          </div>
        </div>
      )
    else return <Redirect to="/dashboard" />
  }
}

export default Rating
