export const SIDENAV_STRUCTURE = {
  "Representative's Details": {
    'Personal Information': {
      'Name of the legal representative': 'Aishwarya',
      Gender: 'Female',
      'Phone Number': 8105526688,
      RFC: '1234567890',
      curp: '123456789198765432',
      'Key of the document that accredits the legal representative or agent':
        'kdhfh67',
      "Representative's name": 'Sharan',
      'Character of the Representative': 'something',
      'Document number that accredits the legal representative': 'hdhjs',
      'Date on which the document was signed': '2018-01-11',
      'Notary number that signs the document': 'dhsdhk',
      'Name of notary who signs the document': 'hjshajhs',
      'City and state where the document was signed': 'something'
    },
    'Schooling Data': {
      Primary: '1',
      'High school': '2',
      'High school or technical career': '3',
      Professional: '4',
      Postgraduate: '5'
    },
    'Substitute for the Beneficiary': {
      'Last name': 'something',
      "Mother's last name": 'something',
      'Name (s)': 'something',
      Gender: 'something',
      curp: 'kjdhkfhkfhkdfh',
      Street: 'something',
      'Outdoor Number': 'something',
      'Interior number': 'something',
      'Town or municipality': 'something',
      State: 'something',
      'Postal Code': 'something'
    }
  },
  'Information of the Society': {
    'General Data of the Business Name': {
      'Name of the company name': 'dsfsfdsf',
      'Name of the Organization of the Agrifood Sector that Belong the Social Name in its Case':
        'hkjhkhk',
      'Constitution date': '2018-01-11',
      'Act number': 334242,
      'Social Object according to the Program': 'jhkhhhhj'
    },
    'Institutional Aspects of the Proponent': {
      Background: 'hkhkhkh',
      'Mission and vision': 'jhkjhkhkh',
      Infrastructure: 'hkhkhjh',
      'Intelectual skills': 'alsjdj'
    },
    'Address of the Corporate Name': {
      Street: 'lfjldjljkl',
      'Outdoor Number': 'hjlkjj',
      'Interior number': 'khkhjhj',
      'Town or municipality': 'alhkakhkhkh',
      State: 'sadasdhhjhghg',
      'Postal Code': 'hfkhskdhjkh',
      Email: 'hsadjhkhadkhkd',
      'Phone number': 889798789
    }
  },
  'Project Data': {},
  'Project Documentation': {},
  Documents: {}
}

export const ASPECT_STRUCTURE = {
  Qualification: [
    'Contribution to the Component',
    'Viability for project deliverables',
    'Relevance based on crop of interest previously supported',
    'Support concepts',
    'Viability for project fullfillment',
    'Contribution to the Component',
    'relevance based on scientific, technical and administrative capacity',
    'Relevant Budget'
  ],
  'Technical Evaluation': [
    'General Data of the Project',
    'Institutional aspects of the proposer',
    'Goals',
    'Problem Statement',
    'Justification',
    'Development of the Productive process, Marketing and / or Conservation',
    'Period of Execution',
    'Financial analysis',
    'Results and Expected Impacts',
    'Related Projects',
    'Training of human resources',
    'Description of Concepts and Amounts of Support',
    'Other Participating Institutions',
    'Schedule of activities',
    'Technical and Administrative Strengths of the Project Collaborators'
  ]
}
