import React, { Component } from 'react'

import { ASPECT_STRUCTURE } from './structures'

class Aspect extends Component {
  constructor(props) {
    super(props)
    this.state = {
      aspect: 'Qualification',
      rating: this.initializeRating(),
      comment: this.initializeComment(),
      error: ''
    }

    this.handleClick = this.handleClick.bind(this)
    this.initializeRating = this.initializeRating.bind(this)
    this.initializeComment = this.initializeComment.bind(this)
    this.handleCommentChange = this.handleCommentChange.bind(this)
    this.handleRatingChange = this.handleRatingChange.bind(this)
  }

  handleClick(e) {
    e.preventDefault()
    console.log('idddddd', e.target.id)
    this.setState({ aspect: e.target.id })
  }

  initializeRating() {
    const obj = {}
    Object.keys(ASPECT_STRUCTURE).forEach(aspect => {
      obj[aspect] = {}
      ASPECT_STRUCTURE[aspect].forEach(sub => {
        obj[aspect][sub] = 0
      })
    })
    return obj
  }

  initializeComment() {
    const obj = {}
    Object.keys(ASPECT_STRUCTURE).forEach(aspect => {
      obj[aspect] = {}
      ASPECT_STRUCTURE[aspect].forEach(sub => {
        obj[aspect][sub] = ''
      })
    })
    return obj
  }

  handleCommentChange(e) {
    e.preventDefault()
    const { aspect, comment } = this.state
    const sub = e.target.name
    this.setState({
      comment: {
        ...comment,
        [aspect]: {
          ...comment[aspect],
          [sub]: e.target.value
        }
      }
    })
  }

  handleRatingChange(e) {
    e.preventDefault()
    const { aspect, rating } = this.state
    const sub = e.target.name
    this.setState({
      rating: {
        ...rating,
        [aspect]: {
          ...rating[aspect],
          [sub]: e.target.value
        }
      }
    })
  }

  render() {
    const { aspect, rating, comment } = this.state
    return (
      <div className="Aspect">
        <div className="Aspect__Content">
          <ul className="Aspect__Nav nav nav-tabs">
            <li onClick={this.handleClick} id="Qualification">
              Qualification
            </li>
            <li id="Technical Evaluation" onClick={this.handleClick}>
              Technical Evaluation>
            </li>
          </ul>
          <div className="Aspect__SubAspects">
            {ASPECT_STRUCTURE[aspect].map((sub, index) => (
              <div className="Aspect__jumobotron">
                <p>
                  <u>{sub}</u>
                </p>
                <label htmlFor="email">Rating</label>
                <input
                  className="Login__formInput--block"
                  type="number"
                  name={sub}
                  value={rating[aspect][sub]}
                  onChange={this.handleRatingChange}
                />
                <span>/ 10</span>

                <label htmlFor="password">Comment</label>
                <textarea
                  className="form-control Login__formInput--block"
                  name={sub}
                  value={comment[aspect][sub]}
                  onChange={this.handleCommentChange}
                />
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}

export default Aspect
