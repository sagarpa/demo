import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { Header } from './header'
import { Footer } from './footer'

export const Layout = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={matchProps => (
        <div className="publicLayout">
          <Header />
          <Component {...matchProps} />
          <Footer />
        </div>
      )}
    />
  )
}

export const PrivateLayout = ({ component: Component, ...rest }) => {
  const isLoggedIn = sessionStorage.getItem('isLoggedIn')
  if (isLoggedIn) {
    return <Layout {...rest} component={Component} />
  } else return <Redirect to="/login" />
}
