import React from 'react'
import { Link } from 'react-router-dom'

export const Header = () => (
  <header>
    <div className="headerBar">
      <img
        className=" img img-responsive logo pull-right"
        alt="Sagarpa Logo"
        src="https://s3.ap-south-1.amazonaws.com/sagarpaorchestratorlocal/asset/logogrey.png"
      />
      <nav className="navbar">
        <div className="navbar-header">
          <button
            type="button"
            className="navbar-toggle"
            data-toggle="collapse"
            data-target=".navbar-collapse"
          >
            <span className="icon-bar" />
            <span className="icon-bar" />
            <span className="icon-bar" />
          </button>
        </div>
        <div className="navbar-collapse collapse">
          <ul className="nav navbar-nav navbar-right">
            <li>
              <Link to="/dashboard">DashBoard</Link>
            </li>
            <li>
              <Link to="/">My Projects</Link>
            </li>
            <li>
              <Link to="/">Reports</Link>
            </li>
            <li>
              <Link to="/">Profile</Link>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </header>
)
