import { Header } from './header'
import { Footer } from './footer'
import { Layout, PrivateLayout } from './layout'

export { Header, Footer, PrivateLayout }
export default Layout
