export const PROJECT_LIST_STRUCTURE = {
  green: [
    {
      projectId: '1',
      State: 'Mexico',
      Kind: 'society',
      Folio: '1009638968',
      'First Name': 'Aishwarya',
      'Paternal Last Name': 'Chaturvedi',
      'Maternal Last Name': 'Mishra',
      curp: 'HDGJDG1568745',
      rfc: 'GVFHN15FR7D5F',
      Evaluator: 'Sharan'
    },
    {
      projectId: '2',
      State: 'Mexico1',
      Kind: 'Person',
      Folio: '1009638318',
      'First Name': 'Kushal',
      'Paternal Last Name': 'GC',
      'Maternal Last Name': 'GC',
      curp: 'HDG45T1568745',
      rfc: 'GVF8I5FR7D5F',
      Evaluator: 'Shravan'
    },
    {
      projectId: '3',
      State: 'Mexico1',
      Kind: 'Person',
      Folio: '1009638318',
      'First Name': 'Joel',
      'Paternal Last Name': 'GC',
      'Maternal Last Name': 'GC',
      curp: 'HDG45T1468745',
      rfc: 'GVF8I5FR7D56',
      Evaluator: 'Shravan'
    },
    {
      projectId: '4',
      State: 'Mexico1',
      Kind: 'Society',
      Folio: '1009638318',
      'First Name': 'John',
      'Paternal Last Name': 'GC',
      'Maternal Last Name': 'GC',
      curp: 'HDG45T1568745',
      rfc: 'GVA8I5FR7D5F',
      Evaluator: 'Shravan'
    },
    {
      projectId: '5',
      State: 'Mexico1',
      Kind: 'Society',
      Folio: '1009638318',
      'First Name': 'Dave',
      'Paternal Last Name': 'GC',
      'Maternal Last Name': 'GC',
      curp: 'HDG46T1568745',
      rfc: 'GVF8IXFR7D5F',
      Evaluator: 'Shravan'
    }
  ],
  red: [
    {
      projectId: '6',
      State: 'Mexico1',
      Kind: 'Person',
      Folio: '1009638318',
      'First Name': 'Kushal',
      'Paternal Last Name': 'GC',
      'Maternal Last Name': 'GC',
      curp: 'HDG45T1568745',
      rfc: 'GVF8I5FR7D5F',
      Evaluator: 'Shravan'
    },
    {
      projectId: '7',
      State: 'Mexico1',
      Kind: 'Person',
      Folio: '1009638318',
      'First Name': 'Joel',
      'Paternal Last Name': 'GC',
      'Maternal Last Name': 'GC',
      curp: 'HDG45T1468745',
      rfc: 'GVF8I5FR7D56',
      Evaluator: 'Shravan'
    },
    {
      projectId: '8',
      State: 'Mexico1',
      Kind: 'Society',
      Folio: '1009638318',
      'First Name': 'John',
      'Paternal Last Name': 'GC',
      'Maternal Last Name': 'GC',
      curp: 'HDG45T1568745',
      rfc: 'GVA8I5FR7D5F',
      Evaluator: 'Shravan'
    }
  ],
  yellow: [],
  inprogress: [],
  unassigned: []
}

export const FILTER_TYPES = [
  'all',
  'red',
  'green',
  'yellow',
  'unassigned',
  'inprogress'
]

// status, folio number,
//
// state, acuse, kind, folio, first name, paternal last name, maternal last name, curp, rfc,
// EVALUATOR,
