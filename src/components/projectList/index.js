import React, { Component } from 'react'
import underscore from 'underscore'

import { PROJECT_LIST_STRUCTURE } from './structures'
import RenderProjectList from './renderProjectList'
import FilterByType from './filterByType'

class ProjectList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      type: ''
    }
    this.filterProjects = this.filterProjects.bind(this)
    this.handleTypeChange = this.handleTypeChange.bind(this)
    this.filterListHeaders = this.filterListHeaders.bind(this)
  }

  filterProjects() {
    const { type } = this.state
    if (type === 'all') {
      const projects = Object.keys(PROJECT_LIST_STRUCTURE).map(
        k => PROJECT_LIST_STRUCTURE[k]
      )
      return underscore.flatten(projects)
    }
    return PROJECT_LIST_STRUCTURE[type]
  }

  handleTypeChange(e) {
    this.setState({ type: e.target.value })
  }

  filterListHeaders() {
    const { type } = this.state
    let project = {}
    let headers = []
    if (type === 'all') {
      Object.keys(PROJECT_LIST_STRUCTURE).forEach(k => {
        if (PROJECT_LIST_STRUCTURE[k].length !== 0) {
          project = PROJECT_LIST_STRUCTURE[k][0]
          return false
        }
      })
    } else {
      const projects = PROJECT_LIST_STRUCTURE[type]
      if (projects.length === 0) headers = []
      else project = projects[0]
    }

    if (Object.keys(project).length === 0) return []
    Object.keys(project).forEach(k => {
      if (k === 'projectId') headers.push('SNo.')
      else headers.push(k)
    })
    return headers
  }

  componentWillMount() {
    const { match: { params: { id } } } = this.props
    this.setState({ type: id })
  }

  render() {
    const { type } = this.state
    return (
      <div className="ProjectList">
        <FilterByType type={type} handleTypeChange={this.handleTypeChange} />
        {type && (
          <div className="ProjectList__List">
            <RenderProjectList
              headersList={this.filterListHeaders()}
              projects={this.filterProjects()}
            />
          </div>
        )}
      </div>
    )
  }
}

export default ProjectList
