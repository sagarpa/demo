import React from 'react'
import PropTypes from 'prop-types'

import { FILTER_TYPES } from './structures'

const FilterByType = ({ type, handleTypeChange }) => (
  <div>
    <h4>Select Category type</h4>
    {FILTER_TYPES.length === 0 ? (
      <select>
        <option>No option available</option>
      </select>
    ) : (
      <select name="filterType" onChange={handleTypeChange} value={type}>
        <option disabled>Select An option</option>
        {FILTER_TYPES.map((type, index) => (
          <option key={index} value={type}>
            {type}
          </option>
        ))}
      </select>
    )}
  </div>
)

FilterByType.propTypes = {
  type: PropTypes.string.isRequired,
  handleTypeChange: PropTypes.func.isRequired
}

export default FilterByType
