import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

const RenderProjectList = ({ headersList, projects }) => {
  return (
    <div>
      {projects.length === 0 ? (
        <h4>No Projects in this category</h4>
      ) : (
        projects.map((project, index) => {
          return (
            <Link to="/ranking">
              <div key={index} className="col-md-12 Project">
                <div className="row">
                  <div className="col-xs-6">
                    <span className="Project__data">Sl No:</span>
                    <span className="Project__data">{project.projectId}</span>
                  </div>
                  <div className="col-xs-3">
                    <span className="Project__data">State:</span>
                    <span className="Project__data">{project.State}</span>
                  </div>
                  <div className="col-xs-3">
                    <span className="Project__data">Folio:</span>
                    <span className="Project__data">{project.Folio}</span>
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-xs-2 col-xs-offset-1">
                    <span className="Project__small">CURP:</span>
                    <span className="Project__small">{project.curp}</span>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xs-2 col-xs-offset-1">
                    <span className="Project__small">RFC:</span>
                    <span className="Project__small">{project.rfc}</span>
                  </div>
                </div>
                <div className="row">
                  <div className="col-xs-2 col-xs-offset-1">
                    <span className="Project__small">Evaluator:</span>
                    <span className="Project__small">{project.Evaluator}</span>
                  </div>
                </div>
              </div>
            </Link>
          )
        })
      )}
    </div>
  )
}

RenderProjectList.propTypes = {
  headersList: PropTypes.array.isRequired,
  projects: PropTypes.array.isRequired
}

export default RenderProjectList
