import React from 'react'
import logo from '../../logo.svg'

const Home = () => (
  <div className="App">
    <div className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <h2>Welcome to Sagarpa</h2>
    </div>
    <p className="App-intro">We are under 'work in progress'</p>
  </div>
)

export default Home
