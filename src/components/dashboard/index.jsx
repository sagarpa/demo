import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import PieGraph from '../chart'
import Legend from './table'

class Dashboard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      category: ''
    }
    this.handleSectorClick = this.handleSectorClick.bind(this)
  }

  handleSectorClick(id) {
    this.setState({ category: id })
  }

  render() {
    const { category } = this.state
    if (category === '')
      return (
        <div className="Dashboard">
          <div className="Dashboard__Content">
            <div className="row">
              <div className="col-md-6">
                <div className="Dashboard__Chart">
                  <PieGraph onClick={this.handleSectorClick} />
                </div>
              </div>
              <div className="col-md-6">
                <div className="Dashboard__Legend">
                  <Legend />
                </div>
              </div>
            </div>
          </div>
        </div>
      )
    else {
      const pathname = '/list/' + category
      return <Redirect to={pathname} />
    }
  }
}

export default Dashboard
