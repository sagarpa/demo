import React, { Component } from 'react'

class Legend extends Component {
  render() {
    return (
      <div>
        <table className="Dashboard__Table table table-bordered">
          <thead className="Dashboard__Table__Head">
            <tr>
              <td colSpan="3">Folios recibidos: 2586</td>
            </tr>
            <tr>
              <td colSpan="3">Folios evaluados: 2086</td>
            </tr>
            <tr>
              <td colSpan="3">Bolsa Asignada: $1,600,000,000.00</td>
            </tr>
            <tr>
              <td>Semáforo</td>
              <td>Folios</td>
              <td>Montos</td>
            </tr>
          </thead>
          <tbody className="Dashboard__Table__Body">
            <tr className="Dashboard__Table__Row Dashboard__Table__Row--Green">
              <td>GREEN</td>
              <td>1000</td>
              <td>$5,800,449,042.70</td>
            </tr>
            <tr className="Dashboard__Table__Row Dashboard__Table__Row--Yellow">
              <td>YELLOW</td>
              <td>400</td>
              <td>$ 4,551,131.18</td>
            </tr>
            <tr className="Dashboard__Table__Row Dashboard__Table__Row--Red">
              <td>RED</td>
              <td>392</td>
              <td>$ 418,704,069.60</td>
            </tr>
            <tr className="Dashboard__Table__Row Dashboard__Table__Row--InProgress">
              <td>INPROGRESS</td>
              <td>294</td>
              <td>$1,507,496,168.00</td>
            </tr>
            <tr className="Dashboard__Table__Row Dashboard__Table__Row--Unassigned">
              <td>UNASSIGNED</td>
              <td>500</td>
              <td>$5,800,449,042.70</td>
            </tr>
            <tr className="Dashboard__Table__Row Dashboard__Table__Row--Footer">
              <td colSpan="3">Bolsa a repartir $1,181,295,930.40</td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}

export default Legend
