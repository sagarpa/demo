import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'

import Layout, { PrivateLayout } from './_base'
import Login from './login/login'
import Dashboard from './dashboard'
import Rating from './rating'
import ProjectList from './projectList'

const ValidatorRouter = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" render={() => <Redirect to="/dashboard" />} />
        <Layout exact path="/login" component={Login} />
        <PrivateLayout exact path="/dashboard" component={Dashboard} />
        <PrivateLayout exact path="/ranking" component={Rating} />
        <PrivateLayout exact path="/list/:id" component={ProjectList} />
      </Switch>
    </Router>
  )
}

export default ValidatorRouter
