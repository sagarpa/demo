import React from 'react'
import { Redirect } from 'react-router-dom'

import { fetchPOST } from '../../helpers/utils'

export default class Login extends React.Component {
  constructor(props) {
    super(props)
    this.assignIsLoggedIn = this.assignIsLoggedIn.bind(this)

    this.state = {
      email: '',
      password: '',
      isLoggedIn: this.assignIsLoggedIn()
    }

    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.setIsLoggedIn = this.setIsLoggedIn.bind(this)
  }

  assignIsLoggedIn() {
    return sessionStorage.getItem('isLoggedIn')
  }

  handleChange(e) {
    e.preventDefault()
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  setIsLoggedIn(url, body) {
    fetchPOST({ url, body })
      .then(response => {
        console.log('response inside login fetch', response)
        sessionStorage.setItem('isLoggedIn', true)
        this.setState({ isLoggedIn: true })
      })
      .catch(err => console.error('Login', err))
  }

  handleSubmit(e) {
    e.preventDefault()
    sessionStorage.setItem('isLoggedIn', true)
    this.setState({
      isLoggedIn: true
    })
    /*
    const { email, password } = this.state
    const url = process.env.REACT_APP_SERVER + '/login'
    const body = { email, password }
    this.setIsLoggedIn(url, body)
    */
  }

  render() {
    const { isLoggedIn } = this.state
    if (!isLoggedIn) {
      return (
        <div className="Login">
          <div className="row Login__Content">
            <div className="col-md-7 ">
              <div className="Login_Maintext">
                <h1 className="Login_Maintext__text Login_Maintext__text--heading">
                  Bienvenido al Acceso Único.
                </h1>
                <hr className="Login_Maintext__text Login_Maintext__text--hr" />
                <p className="Login_Maintext__text Login_Maintext__text--para">
                  Sistema Web que tiene la finalidad proporcionar un acceso
                  único a sistemas de operación de los programas, componentes o
                  proyectos transversales.
                </p>
              </div>
            </div>
            <div className="col-md-5">
              <div className="Login__Jumobotron">
                <p>
                  <u>Login</u>
                </p>
                <form className="" onSubmit={this.handleSubmit}>
                  <label htmlFor="email">Email</label>
                  <input
                    className="form-control Login__formInput--block"
                    type="email"
                    name="email"
                    placeholder="email"
                    onChange={this.handleChange}
                  />

                  <label htmlFor="password">Password</label>
                  <input
                    className="form-control Login__formInput--block"
                    type="password"
                    name="password"
                    placeholder="password"
                    onChange={this.handleChange}
                  />
                  <input
                    className="form-control Login__formButton Login__formInput--submit"
                    type="submit"
                  />
                </form>
              </div>
            </div>
          </div>
        </div>
      )
    } else return <Redirect to="dashboard" />
  }
}
