import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { PieChart, Pie, Cell } from 'recharts'

const RADIAN = Math.PI / 180

class PieGraph extends Component {
  static propTypes = {
    onClick: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)
    this.calculateLabel = this.calculateLabel.bind(this)
    this.handleClick = this.handleClick.bind(this)
    this.state = {
      data: [
        { _id: 'green', name: 'GREEN', value: 1000, color: '#2ecc71' },
        { _id: 'yellow', name: 'YELLOW', value: 400, color: '#f1c40f' },
        { _id: 'red', name: 'RED', value: 392, color: '#c0392b' },
        {
          _id: 'inprogress',
          name: 'In Progress',
          value: 294,
          color: '#3498db'
        },
        { _id: 'unassigned', name: 'Unassigned', value: 500, color: '#7f8c8d' }
      ]
    }
  }

  handleClick(sector) {
    this.props.onClick(sector._id)
  }

  calculateLabel(val) {
    const { cx, cy, midAngle, innerRadius, outerRadius, percent } = val
    const radius = innerRadius + (outerRadius - innerRadius) * 0.5
    const x = cx + radius * Math.cos(-midAngle * RADIAN)
    const y = cy + radius * Math.sin(-midAngle * RADIAN)

    return (
      <text
        x={x}
        y={y}
        fill="white"
        textAnchor={x > cx ? 'start' : 'end'}
        dominantBaseline="central"
      >
        {`${(percent * 100).toFixed(0)}%`}
      </text>
    )
  }

  render() {
    const { data } = this.state
    return (
      <div className="PieGraph">
        <PieChart width={800} height={500}>
          <Pie
            dataKey="value" //TODO: use meaningful dataKey value
            data={data}
            cx={300}
            cy={250}
            labelLine={false}
            label={this.calculateLabel}
            onClick={this.handleClick}
            outerRadius={200}
            fill="#8884d8"
          >
            {data.map((entry, index) => (
              <Cell key={index} id={entry._id} fill={entry.color} />
            ))}
          </Pie>
        </PieChart>
      </div>
    )
  }
}

export default PieGraph
